source 'https://rubygems.org'

gem 'rails', '4.1.8'

gem 'rails-api'

gem 'spring', group: :development

gem 'mysql2'

# Use Linters
gem 'rubocop'

# To use file upload functionality
gem "paperclip", "~> 4.2"

# To seed the project
gem 'seed_dump'

# To serialize the models into Json response
gem 'active_model_serializers'

# Handle API versions
gem 'versionist'

# Mandrill gem for Mandrill API
gem 'mandrill-api'

# Server for concurrence
gem 'puma'

# Geo support
gem 'geocoder'

# To use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
end

group :test do
  gem 'faker'
  gem 'capybara'
  gem 'guard-rspec'
  gem 'launchy'
  gem 'shoulda-matchers'
end

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano', :group => :development

# To use debugger
# gem 'ruby-debug19', :require => 'ruby-debug'
