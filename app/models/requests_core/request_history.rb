class RequestHistory < ActiveRecord::Base
  validates :message, presence:
                      { message:
                          'Debe escribir un cuerpo de mensaje'
                      }
  validates :request, presence:
                      { message:
                          'Debe pertenecer a una solicitud'
                      }
  validates :sender, presence:
                     {message:
                        'Debe tener un autor'
                   }
  belongs_to :request
  belongs_to :sender, polymorphic: true
end
