class Request < ActiveRecord::Base

  validates :manager, presence:
                      { message:
                          'Debe provenir de un cliente'
                      }
  validates :administrator, presence:
                            { message:
                                'Debe ser asignado a un administrador'
                            }
  validates :request_type, presence:
                            {message:
                              'Debe pertenecer a un tipo de solicitud'
                            }

  belongs_to :manager, class_name: 'ManagerProfile'
  belongs_to :administrator, class_name: 'AdministratorProfile'
  belongs_to :request_type
  has_many :request_histories
end
