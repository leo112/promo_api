class Subscription < ActiveRecord::Base
  validates :name, presence:
                   { message:
                       'La subscripcion debe tener nombre'
                   }
  validates :price, format:
                    { with:
                        /\A\d+(?:\.\d{0,2})?\z/,
                      message: 'El precio debe ser un decimal'
                    },
            numericality:
              { message:
                  'El precio debe ser un numero'
              },
            presence:
              { message:
                  'El precio es requerido'
              }
  has_many :subscription_details
end
