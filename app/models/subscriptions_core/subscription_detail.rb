class SubscriptionDetail < ActiveRecord::Base
  before_save :set_expiration_date, :set_current
  before_validation :set_default_price
  include Filterable

  validates :price, format:
                    { with:
                        /\A\d+(?:\.\d{0,2})?\z/,
                      message: 'El precio debe ser un decimal'
                    }
  validates :subscription, presence:
                           {message:
                             'La factura debe tener una subcripcion asociada'
                         }
  belongs_to :manager_profile
  belongs_to :subscription

  def set_current
    self.current=true if self.expiration_date > self.date_owned && SubscriptionDetail.where('current=true') == []
  end

  def self.set_all_to_false
    SubscriptionDetail.all.each do |record|
        record.current = false
        record.save
    end
  end

  def set_expiration_date
    self.expiration_date = Date.today + 30.days
  end

  def set_default_price
    self.price = subscription.price if self.price.nil?
  end
end
