class Administrator < User
  belongs_to :profile, polymorphic: true
end
