class Client < User
  belongs_to :profile, polymorphic: true
  after_save :set_demographic_group
  validates :birth_date, presence:{
                  message: 'La fecha de nacimiento es necesaria'
                }
  validates :sex, presence:{
                  message: 'El genero es necesario'
                }
  private
  def set_demographic_group
    groups = DemographicGroup.all
    for group in groups
      if age >= group.min_age and age <= group.max_age
          if self.sex == group.sex
            self.profile.demographic_group = group
            self.profile.save
          end
      end
    end
  end




end
