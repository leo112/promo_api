class User < ActiveRecord::Base
  has_secure_password
  belongs_to :profile, polymorphic: true
  # validates :username , presence:{
  #                      message: "Porfavor ingrese un nombre de usuario"
  #                    },
  #          uniqueness:{
  #            message: "El nombre de usuario ya ha sido tomado"
  #          }
  # validates :email , presence:{
  #                   message: "El email es necesario"
  #                  },
  #           uniqueness:{
  #             message: "Este email ya esta en uso"
  #           }
  def self.authenticate(username, password)
    user = find_by_username(username)
    user.authenticate(password) if user
  end

  def age
    age = (Date.today().year - self.birth_date.year)
    if self.birth_date.month > Date.today().month
      if self.birth_date.day > Date.today().day
        age = age - 1
      end
    end
    age
  end
end
