class Category < ActiveRecord::Base
  has_many :children, class_name: 'Category', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Category'
  has_many :products
  has_and_belongs_to_many :market_changes
  has_and_belongs_to_many :demographic_groups
  include Filterable

  validates :name, presence:
                   { message:
                       'La categoria debe tener un nombre'
                   }
  scope  :be_parent,lambda { where("parent_id is null")}

  def self.middle
    categories = Category.be_parent
    middle = []
    categories.each do |parent|
      parent.children.each do |child|
        middle << child
      end
    end
    middle
  end

  def self.deep_childs(parent)
    result = []
    parent.children.each do |child|
      if not (child.children === [])
        child.children.each do |leaf|
          result << leaf
        end
      else
        result << child
      end
    end
    result
  end

end
