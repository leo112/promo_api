class Shop < ActiveRecord::Base
  geocoded_by :address
  has_and_belongs_to_many :products
  belongs_to :manager_profile

  after_validation :geocode, :if => :address_changed?
  validates :name, presence:
                   { message:
                       'El local debe tener un nombre'
                   }

  validates :description, presence:
                          { message:
                              'Cuentanos algo de este local'
                          }
  validates :address, presence:
                          {message:
                            'La direccion es requerida para una mejor recomendacion'
                          }


end
