class Product < ActiveRecord::Base
  has_and_belongs_to_many :shops , dependent: :destroy
  belongs_to :manager_profile, dependent: :destroy
  belongs_to :category
  has_many :anonymous_data, class_name: 'AnonymousData'
  has_and_belongs_to_many :preferred, class_name: 'ClientProfile'

  validates :name, presence:
                     { message:
                         'El producto debe tener un nombre'
                     }
  validates :expiration_date, presence:
                                {message:
                                  'Debe introducir una fecha de caducidad de la promocion'

                                }
  validates :description, presence:
                            { message:
                                'El producto debe tener una descripcion'
                            }
  validates :category, presence: { message:
                                     'El producto debe tener una categoria'
                                 }
  validates :price, format:
                      { with:
                            /\A\d+(?:\.\d{0,2})?\z/,
                        message: 'El precio debe ser un decimal'
                      },
                    numericality:
                      { message:
                          'El precio debe ser un numero'
                      }

  validates :discount, format:
                        { with:
                          /\A\d+(?:\.\d{0,2})?\z/,
                          message:
                            'El descuento debe ser un decimal'
                        },
                       numericality:
                         { message:
                             'El descuento debe ser un numero'
                           }
  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/assets/no_product.jpg"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/


end
