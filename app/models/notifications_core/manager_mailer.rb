class ManagerMailer
  require 'mandrill'

  def self.welcome_message(manager)
    m = Mandrill::API.new '6UDtg5GPFFCLoVaWnCbGRg'

    # Prepare the merge vars
    merge_vars = [
      {
        name: 'name',
        content: manager.first_name
      },
      {
        name: 'user_type',
        content: manager.type
      },
      {
        name: 'alias',
        content: 'ArtistMecca Alias'
      }
    ]

    # Render the template
    result = m.templates.render 'Welcome Manager', [], merge_vars

    # Build the message
    message = {
      from_email: 'no-reply@promo.com',
      subject: 'Welcome',
      to: [
        email: 'leonardocelis112@gmail.com' #manager.email
      ],
      html: result['html']
    }

    # Send the message
    sending = m.messages.send message

  end
end