class AnonymousData < ActiveRecord::Base
  validates :person_age, presence:
                         { message:
                             'La edad de la persona es necesaria'
                         }
  validates :person_sex, presence:
                         { message:
                             'El sexo de la persona es necesario'
                         }
  validates :product, presence:
                      { message: 'El dato debe ir referenciado a un producto'
                      }
  belongs_to :product
end
