class Suggestion

  def self.tree_suggestions(client)
    result = tree_categories(client)
    suggestions = []
    result.each do |category|
       suggestions.concat Product.where(category_id:category.id).where(published:true)
    end
    suggestions
  end

  def self.demographic_suggestion(client)
    result = demographic_categories(client)
    suggestions = []
    result.each do |category|
      suggestions.concat Product.where(category_id:category.id).where(published:true)
    end
    suggestions
  end


  def self.tree_categories(client)
    results = []
    client.profile.preferences.each do |preference|
      results.concat Category.deep_childs(preference.category.parent)
    end
    results = results.uniq
  end

  def self.demographic_categories(client)
    results = []
    client.profile.demographic_group.categories.each do |category|
      if Category.deep_childs(category) != []
        results.concat Category.deep_childs(category)
      else
        results << category
      end
    end
    results = results.uniq
  end

end