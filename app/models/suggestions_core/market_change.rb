class MarketChange < ActiveRecord::Base
  has_and_belongs_to_many :categories
  validates :min_age, presence:
                      { message:
                          'Debe contener edad minima'
                      },
            numericality:
              { message:
                  'la edad minima debe ser un numero'
              }
  validates :max_age, presence:
                      { message:
                          'Debe contener edad maxima'
                      },
            numericality:
              { message:
                  'la edad maxima debe ser un numero'
              }
  validates :sex, presence:
                  { message:
                      'Debe tener un sexo'
                  }
end
