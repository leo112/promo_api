class AdministratorProfile < ActiveRecord::Base
  has_one :user, as: :profile, dependent: :destroy
  has_many :requests
  has_many :request_histories, as: :sender, dependent: :destroy
end
