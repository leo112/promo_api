class ClientProfile < ActiveRecord::Base
  has_one :user, as: :profile, dependent: :destroy
  belongs_to :demographic_group
  has_and_belongs_to_many :preferences, class_name: 'Product'
end
