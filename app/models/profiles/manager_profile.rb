class ManagerProfile < ActiveRecord::Base
  has_one :user, as: :profile, dependent: :destroy
  has_many :shops
  has_many :bills, class_name: 'SubscriptionDetail'
  has_many :requests
  has_many :request_histories, as: :sender, dependent: :destroy
  has_many :products

  has_attached_file :company_logo, styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/assets/no_company.jpg"

  validates_attachment_content_type :company_logo, content_type: /\Aimage\/.*\Z/

end
