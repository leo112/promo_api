class V1::BaseController < V1::ApplicationController
  before_action :authenticate , except: :options


  include ActionController::HttpAuthentication::Basic::ControllerMethods

  rescue_from ActiveRecord::RecordNotFound do |exception|
    message = {
      errors: {
        message: "No se encontro ningun registro con id = #{params[:id]} para el recurso actual"
      }
    }
    render json: message, status: 404
  end

  def options
    render :text => '', :content_type => 'text/plain'
  end

  protected


  def authenticate
    authenticate_basic_auth || render_unauthorized
  end

  def authenticate_basic_auth
    authenticate_with_http_basic do |username, password|
      User.authenticate(username, password)
    end
  end

  def render_unauthorized
    headers['WWW-Authenticate'] = 'Basic realm="Users"'
    response = { message:
                   'Bad Credentials'
               }

    render json: response, status: 401
  end
end
