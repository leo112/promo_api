class V1::Platform::ShopsController < V1::BaseController
  def index
    shops = Manager.find(params[:manager_id]).profile.shops
    render json: shops, status: 200, each_serializer: ShopSerializer
  end
  def show
    shop = Manager.find(params[:manager_id]).profile.shops.find(params[:id])
    render json: shop, status: 200, serializer: ShopSerializer
  end
  def create
    shop = Shop.new
    create_params(params).each  do |key, value|
      shop[key] = value if value.present?
    end
    if shop.valid?
      shop.save
      Manager.find(params[:manager_id]).profile.shops << shop
      render json: shop, status: 200, serializer: ShopSerializer
    else
      render json: shop.errors, status: 404
    end
  end

  def update
    shop = Manager.find(params[:manager_id]).profile.shops.find(params[:id])
    update_params(params).each do |key, value|
      shop[key] = value if value.present?
    end
    if shop.valid?
      shop.save
      render json: shop, status: 200, serializer: ShopSerializer
    else
      render json: shop.errors, status: 404
    end

  end
  def destroy
    Manager.find(params[:manager_id]).profile.shops.delete(Shop.find(params[:id]))
    shop = Shop.find(params[:id])
    shop.delete
    render json: shop, status: 200, serializer: ShopSerializer
  end

  protected

  def create_params (params)
    params.slice(:name, :description, :address)
  end

  def update_params (params)
    params.slice(:name, :description, :address)
  end
end