class V1::Platform::SubscriptionsController < V1::BaseController

  def index
    subscription_list = SubscriptionDetail.where(manager_profile: Manager.find(params[:manager_id]).profile).filter(params.slice(:price,:current))
    render json: subscription_list, status: 200
  end

  def show
    subscription = SubscriptionDetail.where(manager_profile: Manager.find(params[:manager_id]).profile).find(params[:id])
    render json: subscription, status:200
  end

  def create
    detail = SubscriptionDetail.new
    create_params(params).each do |key,value|
      detail[key] = value if value.present?
    end
    detail.manager_profile = Manager.find(params[:manager_id]).profile
    if detail.valid?
      SubscriptionDetail.set_all_to_false
      Manager.find(params[:manager_id]).profile.products.where('published=true').each do |product|
        product.published = false
        product.save
      end
      detail.save
      render json: detail, status:200
    else
      render json: detail.errors, status: 404
    end

  end

  def update

  end

  def destroy

  end

  private

  def create_params(params)
    params.slice(:price, :subscription_id)
  end

end