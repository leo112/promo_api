class V1::Platform::ProductsController < V1::BaseController
  def index
    products = Manager.find(params[:manager_id]).profile.products
    render json: products, status: 200, each_serializer: ProductSerializer
  end

  def show
    product = Manager.find(params[:manager_id]).profile.products.find(params[:id])
    render json: product, status: 200 , serializer: ProductSerializer
  end

  def create
    product = Product.new
    create_params(params).each do |key,value|
      if key == 'image'
        product.image = value if value.present?
      else
        product[key] = value if value.present?
      end
    end
    if product.valid?
      product.save
      Manager.find(params[:manager_id]).profile.products << product
      render json: product, status: 200
    else
      render json: product.errors, status: 404
    end
  end

  def update
    product = Manager.find(params[:manager_id]).profile.products.find(params[:id])
    update_params(params).each do |key,value|
      if key == 'image'
        product.image = value if value.present?
      else
        product[key] = value if value.present?
      end
    end
    if product.valid?
      product.save
      render json: product, status: 200
    else
      render json: product.errors, status: 404
    end
  end

  def destroy
    product = Product.find(params[:id])
    Manager.find(params[:manager_id]).profile.products.delete(product)
    product.delete
    render json:product, status: 200, serializer: ProductSerializer
  end

  private
  def create_params(params)
    params.slice(:name, :description, :price, :discount, :expiration_date, :category_id, :image)
  end
  def update_params(params)
    params.slice(:name, :description, :price, :discount, :expiration_date, :category_id, :image)
  end
end