class V1::Platform::CategoriesController < V1::BaseController
  def index
    categories = Category.filter(params.slice(:none))
    categories = Category.be_parent if params[:parents].present?
    categories = Category.middle if params[:middle].present?
    render json:categories, status: 200, each_serializer: CategorySerializer
  end

  def show
    category = Category.find(params[:id])
    render json: category, status: 200, serializer: CategorySerializer
  end

  def create
    category = Category.new
    create_params(params).each do |key,value|
      category[key]= value if value.present?
    end
    if category.valid?
      category.save
      render json: category, status: 200, serializer: CategorySerializer
    else
      render json: category.errors, status: 401
    end
  end

  def update
    category = Category.find(params[:id])
    update_params(params).each do |key,value|
      category[key]= value if value.present?
    end
    if category.valid?
      category.save
      render json: category, status: 200, serializer: CategorySerializer
    else
      render json: category.errors, status: 401
    end
  end

  def delete
    category = Category.find(params[:id])
    category.delete
    render json: {message:'category successfully deleted'}
  end

  private

  def create_params(params)
    params.slice(:name,:parent_id)
  end

  def update_params(params)
    params.slice(:name,:parent_id)
  end
end