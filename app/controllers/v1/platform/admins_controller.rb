class V1::Platform::AdminsController < V1::BaseController
  def index
    admins = Administrator.all
    render json: admins, status: 200, each_serializer: UserSerializer
  end

  def create
    admin = Administrator.new
    create_params(params).each do |key,value|
      if key === 'password'
        admin.password = value if value.present?
      else
        admin[key] = value if value.present?
      end
    end
    if admin.valid?
      admin.save
      render json: admin, status: 200, serializer: UserSerializer
    else
      render json: admin.errors, status: 404
    end
  end

  def update
    admin = Administrator.find(params[:id])
    update_params(params).each do |key,value|
      if key === 'password'
        admin.password = value if value.present?
      else
        admin[key] = value if value.present?
      end
    end
    if admin.valid?
      admin.save
      render json: admin, status: 200, serializer: UserSerializer
    else
      render json: admin.errors, status: 404
    end
  end

  def show
    admin = Administrator.find(params[:id])
    render json: admin, status: 200, serializer: UserSerializer
  end

  def destroy

  end
end