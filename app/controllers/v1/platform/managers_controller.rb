class V1::Platform::ManagersController < V1::BaseController
  before_action :authenticate ,except: [:options,:create]
  def index
    managers = Manager.all
    render json: managers, status: 200, each_serializer: ManagerSerializer
  end

  def create
    manager = Manager.new
    create_params(params).each do |key,value|
      if key === 'password'
        manager.password = value if value.present?
      else
        manager[key] = value if value.present?
      end
    end
    manager.profile = ManagerProfile.new
    if manager.valid?
      manager.save
      ##ManagerMailer.welcome_message(manager)
      render json: manager, status: 200, serializer: ManagerSerializer
    else
      render json: manager.errors, status: 404
    end
  end

  def update
    manager = Manager.find(params[:id])
    update_params(params).each do |key,value|
      if key === 'password'
        manager.password = value if value.present?
      elsif key === 'company_logo'
        manager.profile.company_logo = value if value.present?
        manager.profile.save
      elsif key ==  'company_name'
        manager.profile.company_name = value if value.present?
        manager.profile.save
      else
        manager[key] = value if value.present?
      end
    end
    if manager.valid?
      manager.save
      render json: manager, status: 200, serializer: ManagerSerializer
    else
      render json: manager.errors, status: 404
    end
  end

  def show
    manager = Manager.find(params[:id])
    render json: manager, status: 200, serializer: ManagerSerializer
  end

  def destroy

  end
  protected

  def create_params(params)
    params.slice(:first_name, :last_name, :password, :age, :email, :username, :company_logo, :company_name)
  end

  def update_params(params)
    params.slice(:first_name, :last_name, :password, :age, :email, :username, :company_logo, :company_name)
  end
end