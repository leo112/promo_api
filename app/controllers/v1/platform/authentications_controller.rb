class V1::Platform::AuthenticationsController < V1::BaseController
  skip_before_action :authenticate
  def index

  end
  def verify_credentials
    # Get user from request
    @current_user = Manager.authenticate(params[:username], params[:password])

    if @current_user
      # Prepare and return verified credentials
      render json: @current_user, status: 200, serializer: ManagerSerializer
    else
      # Render 401 unauthorized - bad credentials
      response = { message:
                     'Nombre de usuario y/o clave incorrectos'
      }
      render json: response, status: 401
    end
  end
end
