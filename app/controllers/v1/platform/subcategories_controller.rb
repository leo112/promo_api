class V1::Platform::SubcategoriesController < V1::BaseController
  def index
    subcategories= Category.find(params[:category_id]).children
    render json: subcategories, status: 200, each_serializer: CategorySerializer
  end
end