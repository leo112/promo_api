class V1::Platform::ShopProductsController < V1::BaseController
  def index
    products = Manager.find(params[:manager_id]).profile.shops.find(params[:shop_id]).products
    render json: products, status: 200, each_serializer: ProductSerializer
  end

  def show
    product = Manager.find(params[:manager_id]).profile.shops.find(params[:shop_id]).products.find(params[:id])
    render json: product, status: 200 , serializer: ProductSerializer
  end

  def create
    product = Product.find(params[:product_id])
    shop = Manager.find(params[:manager_id]).profile.shops.find(params[:shop_id])
    if !shop.products.exists?(product)
      shop.products << product
      render json: shop, status: 200, serializer: ShopSerializer
    else
      render json: {message: 'El producto ya existe en esta tienda'} , status: 404
    end
  end

  def destroy
    shop = Manager.find(params[:manager_id]).profile.shops.find(params[:shop_id])
    product = Product.find(params[:id])
    shop.products.delete(product)
    render json: product, status: 200, serializer: ProductSerializer
  end

end