class V1::Platform::PublishController < V1::BaseController

  def create
    product = Product.find(params[:product_id])
    current_subscription = Manager.find(params[:manager_id]).profile.bills.where('current=true')
    maximum_products = current_subscription.first.subscription.allowed_published_products
    published_products = Manager.find(params[:manager_id]).profile.products.where('published=true').count
    if published_products + 1 > maximum_products
      render json: {message:"Tu subscripción no admite la publicación de más productos"}, status: 404
    else
      product.published = true
      product.save
      render json: product , status: 200
    end

  end

  def destroy
    product = Product.find(params[:product_id])
    product.published = false
    product.save
    render json: product
  end

end