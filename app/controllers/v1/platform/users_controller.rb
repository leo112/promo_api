class V1::Platform::UsersController < V1::BaseController
  def index
    users = User.all
    render json: users, status: 200 , each_serializer: UserSerializer
  end

  def create

  end

  def show
    user = User.find(params[:id])
    render json: user, status: 404, serializer: UserSerializer
  end

  def update
    user = User.find(params[:id])
    update_params(params).each do |key,value|
      if key === 'password'
        user.password = value if value.present?
      else
        user[key] = value if value.present?
      end
    end
    if user.valid?
      user.save
      render json: user, status: 200, serializer: UserSerializer
    else
      render json: user.errors, status: 404
    end
  end

  def destroy

  end

  protected

  def update_params(params)
    params.slice(:first_name, :last_name, :password, :birth_date, :email, :sex, :username)
  end

  def create_params(params)
    params.slice(:first_name, :last_name, :password, :birth_date, :email, :sex, :username)
  end

end
