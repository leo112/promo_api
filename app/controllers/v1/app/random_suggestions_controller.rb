class V1::App::RandomSuggestionsController < V1::BaseController
  def index
    suggestions = Product.limit(20).order('RAND()').where(published:true)
    render json: suggestions, status:200, each_serializer: ProductSerializer
  end
end