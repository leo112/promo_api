class V1::App::ProductsController < V1::BaseController
  def show
    product = Product.find(params[:id])
    render json: product, status: 200, serializer: ProductSerializer
  end
end