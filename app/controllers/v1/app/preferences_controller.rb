class V1::App::PreferencesController < V1::BaseController
  def index
    preferences = Client.find(params[:client_id]).profile.preferences.where(published:true)
    render json: preferences, status: 200, each_serializer: ProductSerializer
  end

  def show
    preference = Client.find(params[:client_id]).profile.preferences.find(params[:id])
    render json: preference, status: 200, serializer: ProductSerializer
  end

  def create
    preference = Product.find(params[:product_id])
    Client.find(params[:client_id]).profile.preferences << preference
    render json: preference, status: 200, serializer: ProductSerializer
  end

  def update

  end

  def destroy
    preference = Product.find(params[:id])
    Client.find(params[:client_id]).profile.preferences.delete(preference)
    render json: preference, status: 200, serializer: ProductSerializer
  end

end