class V1::App::TreeSuggestionsController < V1::BaseController
  def index
    user = Client.find(params[:client_id])
    data = Suggestion.tree_suggestions(user)
    render json:data, status: 200,  each_serializer: ProductSerializer
  end
end