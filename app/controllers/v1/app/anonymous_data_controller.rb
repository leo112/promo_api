class V1::App::AnonymousDataController < V1::BaseController
  def create
    record = AnonymousData.new
    create_params(params).each do |key, value|
      record[key] = value if value.present?
    end
    if record.valid?
      record.save
      render json: record, status: 200
    else
      render json: record.errors, status: 404
    end

  end

  private
  def create_params(params)
    params.slice(:person_age, :person_sex, :product_id)
  end
end