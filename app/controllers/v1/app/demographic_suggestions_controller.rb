class V1::App::DemographicSuggestionsController < V1::BaseController
  def index
    user = Client.find(params[:client_id])
    data = Suggestion.demographic_suggestion(user)
    render json:data, status: 200, each_serializer: ProductSerializer
  end
end