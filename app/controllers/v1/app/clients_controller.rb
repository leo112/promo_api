class V1::App::ClientsController <  V1::Platform::UsersController
  before_action :authenticate ,except: [:options,:create]
  def index
    clients = Client.all
    render json: clients, status: 200, each_serializer: ClientSerializer
  end

  def create
    client = Client.new
    create_params(params).each do |key,value|
      if key === 'password'
        client.password = value if value.present?
      else
        client[key] = value if value.present?
      end
    end
    client.profile = ClientProfile.new
    if client.valid?
      client.save
      render json: client, status: 200, serializer: ClientSerializer
    else
      render json: client.errors, status: 404
    end
  end

  def update
    client = Client.find(params[:id])
    update_params(params).each do |key,value|
      if key === 'password'
        client.password = value if value.present?
      else
        client[key] = value if value.present?
      end
    end
    if client.valid?
      client.save
      render json: client, status: 200, serializer: ClientSerializer
    else
      render json: client.errors, status: 404
    end
  end

  def show
    client = Client.find(params[:id])
    render json: client, status: 200, serializer: ClientSerializer
  end

  def destroy

  end
end