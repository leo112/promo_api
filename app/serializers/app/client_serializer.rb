class ClientSerializer < UserSerializer
  attributes :age,
             :birth_date,
             :sex
end