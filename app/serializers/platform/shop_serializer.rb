class ShopSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :address,
             :latitude,
             :longitude,
             :description,
             :products

  def products
    object.products
  end
end