class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :first_name,
             :last_name,
             :type,
             :email,
             :username,
             :profile_type,
             :created_at,
             :updated_at
end