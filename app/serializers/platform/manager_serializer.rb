class ManagerSerializer < UserSerializer
  attributes :company_logo,
             :company_name

  def company_name
    object.profile.company_name
  end
  def company_logo
    object.profile.company_logo.url(:thumb)
  end
end