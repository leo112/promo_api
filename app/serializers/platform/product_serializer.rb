class ProductSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :description,
             :price,
             :discount,
             :expiration_date,
             :category_name,
             :image_url,
             :image_thumb_url,
             :published,
             :expiration_date,
             :shops

  def image_thumb_url
    object.image.url(:thumb)
  end
  def image_url
    object.image.url
  end

  def shops
    object.shops
  end

  def category_name
    Category.find(object.category_id).name
  end
end