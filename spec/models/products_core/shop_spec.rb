require 'rspec'

describe Shop do
  context 'shop creation', product_core: true, unit_test: true do
    it 'should have a valid factory' do
      expect(create(:shop)).to be_valid
    end
    it 'should not be created without a name' do
      expect(build(:shop, name: nil))
        .to validate_presence_of(:name)
              .with_message('El local debe tener un nombre')
    end
    it 'should not be created without a description' do
      expect(build(:shop, description: nil))
        .to validate_presence_of(:description)
              .with_message('Cuentanos algo de este local')
    end
    it 'should not be created without an address' do
      expect(build(:shop, address: nil))
        .to validate_presence_of(:address)
              .with_message('La direccion es requerida para una mejor recomendacion')
    end
  end
end