require 'rspec'

describe Product do
  context 'Product creation', product_core: true, unit_test: true do
    it 'should have a valid stubbed factory' do
      expect(create(:stub_product)).to be_valid
    end

    it 'should not be created without a name' do
      expect(build(:stub_product, name: nil))
        .to validate_presence_of(:name)
        .with_message('El producto debe tener un nombre')
    end

    it 'should not be created without a description' do
      expect(build(:stub_product, description: nil))
        .to validate_presence_of(:description)
        .with_message('El producto debe tener una descripcion')
    end

    it 'should not be created without a category' do
      expect(build(:stub_product, category: nil))
        .to validate_presence_of(:category)
        .with_message('El producto debe tener una categoria')
    end

    it 'should not be created with improper price format' do
      expect(build(:stub_product, price: 12.9999516)).not_to be_valid
    end

    it 'should not be created with a no number price' do
      expect(build(:stub_product, price: 'asdasd'))
        .to validate_numericality_of(:price)
        .with_message('El precio debe ser un numero')
    end

    it 'should not be created with improper discount format' do
      expect(build(:stub_product, discount: 12.9999516)).not_to be_valid
    end

    it 'should not be created with a no number discount' do
      expect(build(:stub_product, discount: 'asdasd'))
        .to validate_numericality_of(:discount)
        .with_message('El descuento debe ser un numero')
    end
  end
  context 'Product relations', product_core: true, integration_test: true do
    it 'should have a category instance' do
      expect(create(:product).category).to be_kind_of(Category)
    end
  end
end
