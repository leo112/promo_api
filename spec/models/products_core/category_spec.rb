require 'rspec'

describe Category do
  context 'category creation', product_core: true, unit_test: true do
    it 'should have a valid factory' do
      expect(create(:category)).to be_valid
    end
    it 'should not be created without a name' do
      expect(build(:category, name: nil))
        .to validate_presence_of(:name)
              .with_message('La categoria debe tener un nombre')
    end
  end
end