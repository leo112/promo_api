require 'rspec'

describe User do
  it 'has a valid factory' do
    expect(create(:user)).to be_valid
  end
  it 'cannot be created without password' do
    expect(build(:user_with_no_password)).not_to be_valid
  end
end
