require 'rspec'

describe AnonymousData do
  context 'anonymous data creation', suggestion_core: true, unit_test: true do
    it 'should have a valid stubbed factory' do
      expect(create(:stub_anonymous_data)).to be_valid
    end
    it 'should not be created without person_age' do
      expect(build(:stub_anonymous_data, person_age: nil))
        .to validate_presence_of(:person_age)
              .with_message('La edad de la persona es necesaria')
    end
    it 'should not be created without person_sex' do
      expect(build(:stub_anonymous_data, person_sex: nil))
        .to validate_presence_of(:person_sex)
              .with_message('El sexo de la persona es necesario')
    end
    it 'should not be created without product' do
      expect(build(:stub_anonymous_data, product: nil))
        .to validate_presence_of(:product)
              .with_message('El dato debe ir referenciado a un producto')
    end
  end
end