require 'rspec'

describe DemographicGroup do
  context 'Demographic group creation', suggestion_core: true, unit_test: true do
    it 'should have a valid factory' do
      expect(create(:demographic_group)).to be_valid
    end
    it 'should not be created without a min_age' do
      expect(build(:demographic_group, min_age: nil))
        .to validate_presence_of(:min_age)
              .with_message('Debe contener edad minima')
    end
    it 'should not be created with improper min_age format' do
      expect(build(:demographic_group, min_age: 'asdasd'))
        .not_to be_valid
    end
    it 'should not be created without a max_age' do
      expect(build(:demographic_group, max_age: nil))
        .to validate_presence_of(:max_age)
              .with_message('Debe contener edad maxima')
    end
    it 'should not be created with improper max_age format' do
      expect(build(:demographic_group, max_age: 'asdasd'))
        .not_to be_valid
    end
  end
end