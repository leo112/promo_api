require 'faker'

FactoryGirl.define do
  factory :shop do |f|
    f.name { Faker::Name.name }
    f.description { Faker::Lorem.sentence }
    f.address {'282 Kevin Cir, Raleigh, North Carolina 27609-5944, Estados Unidos'}
  end
end