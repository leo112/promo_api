require 'faker'
require 'rspec'

FactoryGirl.define do
  factory :product do |f|
    f.name { Faker::Name.name }
    f.description { Faker::Lorem.sentence }
    f.price { Faker::Commerce.price }
    f.expiration_date { '2015-07-20' }
    f.published { false }
    f.discount { 0.16 }
    association :category, factory: :category, name: 'Zapatos'
  end
  factory :stub_product, class: Product do |f|
    f.name { Faker::Name.name }
    f.description { Faker::Lorem.sentence }
    f.price { Faker::Commerce.price }
    f.expiration_date { '2015-07-20' }
    f.published { false }
    f.discount { 0.16 }
    f.category {FactoryGirl.build_stubbed :category}
  end
end
