require 'faker'

FactoryGirl.define do
  factory :stub_anonymous_data, class: AnonymousData do |f|
    f.person_age { 20 }
    f.person_sex { "Masculino" }
    f.product {FactoryGirl.build_stubbed :product}
  end

  factory :anonymous_data do |f|
    f.person_age { 20 }
    f.person_sex { "Masculino" }
    association :product, factory: :product
  end
end