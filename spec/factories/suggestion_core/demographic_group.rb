require 'faker'

FactoryGirl.define do
  factory :demographic_group do |f|
    f.min_age { 20 }
    f.max_age { 25 }
    f.sex     { 'Femenino' }
  end
end
