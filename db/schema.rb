# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150515031840) do

  create_table "administrator_profiles", force: true do |t|
    t.text    "biography"
    t.integer "success_requests"
  end

  create_table "anonymous_data", force: true do |t|
    t.integer "person_age"
    t.string  "person_sex"
    t.boolean "not_demographic", default: true
    t.date    "date_collected",  default: '2015-07-06'
    t.integer "product_id"
  end

  create_table "categories", force: true do |t|
    t.string  "name"
    t.integer "parent_id"
  end

  create_table "categories_demographic_groups", force: true do |t|
    t.integer "demographic_group_id"
    t.integer "category_id"
  end

  add_index "categories_demographic_groups", ["category_id"], name: "index_categories_demographic_groups_on_category_id", using: :btree
  add_index "categories_demographic_groups", ["demographic_group_id"], name: "index_categories_demographic_groups_on_demographic_group_id", using: :btree

  create_table "categories_market_changes", force: true do |t|
    t.integer "category_id"
    t.integer "market_change_id"
  end

  add_index "categories_market_changes", ["category_id"], name: "index_categories_market_changes_on_category_id", using: :btree
  add_index "categories_market_changes", ["market_change_id"], name: "index_categories_market_changes_on_market_change_id", using: :btree

  create_table "client_profiles", force: true do |t|
    t.boolean "use_device",           default: true
    t.integer "demographic_group_id"
  end

  add_index "client_profiles", ["demographic_group_id"], name: "index_client_profiles_on_demographic_group_id", using: :btree

  create_table "client_profiles_products", force: true do |t|
    t.integer "client_profile_id"
    t.integer "product_id"
  end

  add_index "client_profiles_products", ["client_profile_id"], name: "index_client_profiles_products_on_client_profile_id", using: :btree
  add_index "client_profiles_products", ["product_id"], name: "index_client_profiles_products_on_product_id", using: :btree

  create_table "demographic_groups", force: true do |t|
    t.integer "min_age"
    t.integer "max_age"
    t.string  "sex"
  end

  create_table "manager_profiles", force: true do |t|
    t.string   "company_name"
    t.integer  "score"
    t.string   "company_logo_file_name"
    t.string   "company_logo_content_type"
    t.integer  "company_logo_file_size"
    t.datetime "company_logo_updated_at"
  end

  create_table "market_changes", force: true do |t|
    t.integer "min_age"
    t.integer "max_age"
    t.string  "sex"
    t.date    "date_generated", default: '2015-07-06'
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.decimal  "price",              precision: 50, scale: 2
    t.boolean  "published",                                   default: false
    t.decimal  "discount",           precision: 3,  scale: 2
    t.date     "expiration_date"
    t.integer  "category_id"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "manager_profile_id"
  end

  add_index "products", ["manager_profile_id"], name: "index_products_on_manager_profile_id", using: :btree

  create_table "products_shops", force: true do |t|
    t.integer "shop_id"
    t.integer "product_id"
  end

  add_index "products_shops", ["product_id"], name: "index_products_shops_on_product_id", using: :btree
  add_index "products_shops", ["shop_id"], name: "index_products_shops_on_shop_id", using: :btree

  create_table "request_histories", force: true do |t|
    t.integer "sender_id"
    t.string  "sender_type"
    t.text    "message"
    t.integer "request_id"
  end

  add_index "request_histories", ["sender_id", "sender_type"], name: "index_request_histories_on_sender_id_and_sender_type", using: :btree

  create_table "request_types", force: true do |t|
    t.string "name"
  end

  create_table "requests", force: true do |t|
    t.integer "administrator_profile_id"
    t.integer "manager_profile_id"
    t.integer "request_type_id"
    t.boolean "open",                     default: true
    t.date    "date_opened",              default: '2015-07-06'
    t.date    "date_closed"
  end

  add_index "requests", ["administrator_profile_id"], name: "index_requests_on_administrator_profile_id", using: :btree
  add_index "requests", ["manager_profile_id"], name: "index_requests_on_manager_profile_id", using: :btree

  create_table "shops", force: true do |t|
    t.string  "name"
    t.string  "address"
    t.float   "latitude",           limit: 24
    t.float   "longitude",          limit: 24
    t.text    "description"
    t.integer "manager_profile_id"
  end

  add_index "shops", ["manager_profile_id"], name: "index_shops_on_manager_profile_id", using: :btree

  create_table "subscription_details", force: true do |t|
    t.date    "date_owned",                                  default: '2015-07-06'
    t.date    "expiration_date"
    t.boolean "current",                                     default: false
    t.decimal "price",              precision: 10, scale: 2
    t.integer "manager_profile_id"
    t.integer "subscription_id"
  end

  add_index "subscription_details", ["manager_profile_id"], name: "index_subscription_details_on_manager_profile_id", using: :btree
  add_index "subscription_details", ["subscription_id"], name: "index_subscription_details_on_subscription_id", using: :btree

  create_table "subscriptions", force: true do |t|
    t.string  "name"
    t.decimal "price",                              precision: 10, scale: 2
    t.integer "allowed_published_products"
    t.boolean "allowed_to_see_basic_statistics"
    t.boolean "allowed_to_see_detailed_statistics"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "type"
    t.date     "birth_date"
    t.string   "email"
    t.string   "sex"
    t.string   "username"
    t.string   "password_digest"
    t.integer  "profile_id"
    t.string   "profile_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["profile_id", "profile_type"], name: "index_users_on_profile_id_and_profile_type", using: :btree

end
