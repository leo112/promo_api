class CreateAnonymousData < ActiveRecord::Migration
  def change
    create_table :anonymous_data do |t|
      t.integer :person_age
      t.string :person_sex
      t.boolean :not_demographic, default: true
      t.date :date_collected, default: Date.today()
      t.belongs_to :product
    end
  end
end
