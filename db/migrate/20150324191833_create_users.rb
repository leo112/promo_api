class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :type
      t.date :birth_date
      t.string :email
      t.string :sex
      t.string :username
      t.string :password_digest
      t.references :profile, polymorphic: true, index: true
      t.timestamps
    end
  end
end
