class CreateRelationBetweenMarketChangeAndCategory < ActiveRecord::Migration
  def change
    create_table :categories_market_changes do |t|
      t.belongs_to :category, index: true
      t.belongs_to :market_change, index: true
    end
  end
end
