class CreateDemographicGroupAndAJoinTable < ActiveRecord::Migration
  def change
    create_table :demographic_groups do |t|
      t.integer :min_age
      t.integer :max_age
      t.string :sex
    end
    create_table :categories_demographic_groups do |t|
      t.belongs_to :demographic_group, index: true
      t.belongs_to :category, index: true
    end
  end
end
