class CreateClientProfile < ActiveRecord::Migration
  def change
    create_table :client_profiles do |t|
      t.boolean :use_device, default: true
      t.belongs_to :demographic_group , index: true
    end
  end
end
