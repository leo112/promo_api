class AddCreatedByToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.belongs_to :manager_profile, index: true
    end
  end
end
