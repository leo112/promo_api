class CreateSubscriptionCoreRelation < ActiveRecord::Migration
  def change
    create_table :subscription_details do |t|
      t.date :date_owned, default: Date.today
      t.date :expiration_date
      t.boolean :current, default: false
      t.decimal :price, { precision: 10, scale:2}
      t.belongs_to :manager_profile, index: true
      t.belongs_to :subscription, index:true
    end
    create_table :subscriptions do |t|
      t.string :name
      t.decimal :price, { precision: 10, scale:2}
      t.integer :allowed_published_products
      t.boolean :allowed_to_see_basic_statistics
      t.boolean :allowed_to_see_detailed_statistics
    end
  end
end
