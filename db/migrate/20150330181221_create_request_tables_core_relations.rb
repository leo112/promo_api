class CreateRequestTablesCoreRelations < ActiveRecord::Migration
  def change
    create_table :requests do |t|
      t.belongs_to :administrator_profile, index: true
      t.belongs_to :manager_profile, index: true
      t.belongs_to :request_type
      t.boolean :open, default: true
      t.date :date_opened, default: Date.today
      t.date :date_closed
    end
    create_table :request_types do |t|
      t.string :name
    end
    create_table :request_histories do |t|
      t.references :sender, polymorphic: true, index: true
      t.text :message
      t.belongs_to :request
    end
  end
end
