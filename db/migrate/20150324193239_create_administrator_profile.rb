class CreateAdministratorProfile < ActiveRecord::Migration
  def change
    create_table :administrator_profiles do |t|
      t.text :biography
      t.integer :success_requests
    end
  end
end
