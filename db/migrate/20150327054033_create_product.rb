class CreateProduct < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.decimal :price, { precision: 50, scale:2}
      t.boolean :published, default: false
      t.decimal :discount,  { precision: 3, scale: 2 }
      t.date :expiration_date
      t.integer :category_id, index: true
    end
  end
end
