class CreateRelationBetweenProductAndShop < ActiveRecord::Migration
  def change
    create_table :products_shops do |t|
      t.belongs_to :shop, index: true
      t.belongs_to :product, index: true
    end
  end
end
