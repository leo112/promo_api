class AddManagerToShop < ActiveRecord::Migration
  def change
    add_reference :shops, :manager_profile, index: true
  end
end
