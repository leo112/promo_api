class CreatePreferencesJoinTable < ActiveRecord::Migration
  def change
    create_table :client_profiles_products do |t|
      t.belongs_to :client_profile, index: true
      t.belongs_to :product, index: true
    end
  end
end
