class CreateMarketChange < ActiveRecord::Migration
  def change
    create_table :market_changes do |t|
      t.integer :min_age
      t.integer :max_age
      t.string :sex
      t.date :date_generated, default: Date.today
    end
  end
end
