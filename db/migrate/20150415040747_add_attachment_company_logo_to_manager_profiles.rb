class AddAttachmentCompanyLogoToManagerProfiles < ActiveRecord::Migration
  def self.up
    change_table :manager_profiles do |t|
      t.attachment :company_logo
    end
  end

  def self.down
    remove_attachment :manager_profiles, :company_logo
  end
end
