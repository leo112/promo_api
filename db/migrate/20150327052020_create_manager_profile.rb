class CreateManagerProfile < ActiveRecord::Migration
  def change
    create_table :manager_profiles do |t|
      t.string :company_name
      t.integer :score
    end
  end
end
