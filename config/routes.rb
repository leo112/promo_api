Rails.application.routes.draw do
  # The priority is based upon order of
  # creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  api_version(
    module: 'V1',
    path: { value: 'v1' },
    default: true, defaults: { format: 'json' }
  ) do
      namespace :platform do
        match 'users', to: 'users#options', via: [:options]
        resources :users
        match 'managers', to: 'managers#options', via: [:options]
        match 'managers/:id', to: 'managers#options', via: [:options]
        resources :managers do
          match 'shops', to: 'shops#options', via: [:options]
          match 'shops/:id', to: 'shops#options', via: [:options]
          resources :shops do
            match 'products', to: 'shop_products#options', via: [:options]
            match 'products/:id', to: 'shop_products#options', via: [:options]
            resources :products, controller: :shop_products
          end
          match 'products', to: 'products#options', via: [:options]
          match 'products/:id', to: 'products#options', via: [:options]
          resources :products do
            match 'publish', to: 'publish#options', via: [:options]
            resources :publish
            delete 'publish' => 'publish#destroy'
          end
          match 'subscriptions', to: 'subscriptions#options', via: [:options]
          resources :subscriptions
        end
        match 'administrators', to: 'administrators#options', via: [:options]
        resources :admins
        match 'categories', to: 'categories#options', via: [:options]
        resources :categories do
          match 'subcategories', to: 'subcategories#options', via: [:options]
          resources :subcategories
        end
        match 'authentications/verify_credentials', to: 'authentications#options', via: [:options]
        post 'authentications/verify_credentials'
      end
      namespace :app do
        match 'clients', to: 'clients#options', via: [:options]
        resources :clients do
          match 'preferences', to: 'preferences#options', via: [:options]
          resources :preferences
          match 'demographic_suggestions', to: 'demographic_suggestions#options', via: [:options]
          resources :demographic_suggestions
          match 'tree_suggestions', to: 'tree_suggestions#options', via: [:options]
          resources :tree_suggestions
          match 'random_suggestions', to: 'random_suggestions#options', via: [:options]
          resources :random_suggestions
        end
        match 'anonymous_data', to: 'anonymous_data#options', via: [:options]
        resources :anonymous_data
        post 'authentications/verify_credentials'
        match 'products', to: 'products#options', via: [:options]
        resources :products
      end
  end


  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route
  # (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
